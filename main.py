from libsophie import Daemon, Logging, Config


def main():
    logger = Logging.Logging()
    logger.info("Starting Sophie")
    logger.debug("Reading configuration")
    configuration = Config.read_config('config.ini')
    logger.debug("Configuration read")
    logger.debug("Configuration: " + str(configuration))
    logger.info("Sophie started")

    if configuration['Watchers']['Screen'] == 'True':
        logger.info("Starting ScreenWatcher")
        from libsophie.Watchers import Screen

        def screenwatch():
            if Screen.detect_change_new_invert_if_white(change_percent=0.01):
                logger.debug("Screen changed")
                logger.debug("Taking screenshot")
                screenshot = Screen.take_screenshot_bw_resize_np_invert_if_white()
                logger.debug("Screenshot taken")

        screenwatcher = Daemon.Daemon(screenwatch, name='sophie-screenwatcher')
        screenwatcher.run()

    if configuration['Watchers']['Keyboard'] == 'True':
        logger.info("Starting KeyboardWatcher")
        from libsophie.Watchers import Keyboard

        def keyboardwatch():
            while True:
                keys = Keyboard.get_keys()
                if keys:
                    logger.debug("Keys pressed: " + str(keys))

        keyboardwatcher = Daemon.Daemon(keyboardwatch, name='sophie-keyboardwatcher')
        keyboardwatcher.run()


if __name__ == '__main__':
    daemon = Daemon.Daemon(main, name='sophie')
    daemon.run()
